#!/usr/bin/python3
# -*- encoding: utf-8 -*-

"""
compile - Sequentially compile the whole DesQ project

###================== Program Info ==================###
    Program Name : compile
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys
    Python Version : Python 3.4 or higher
    Author : Marcus Britanicus
    Email : marcusbritanicus@gmail.com
    License : Public Domain
###==================================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # This script is in public domain; do whatever you want to do with it.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys, shutil

Projects = [
    'Meta',
    'libdesq',
    'libdesqui',
    'Session',
    'Shell',
    'ShellPlugins',
    'Integration',
    'Theme',
    'Clipboard',
    'Disks',
    "Keyring",
    "Lock",
    'Notifier',
    'PowerManager',
    'Runner',
    'SNI',
    'Volume',
    'Archiver',
    'DropDown',
    'Docs',
    'Term',
    'Eye',
]


ProjectsInfo = {
    'Meta':             ( 'Meta',                 'https://gitlab.com/DesQ/Meta.git' ),
    'libdesq':          ( 'LibDesQ/libdesq',      'https://gitlab.com/DesQ/libdesq.git' ),
    'libdesqui':        ( 'LibDesQ/libdesqui',    'https://gitlab.com/DesQ/libdesqui.git' ),
    'Session':          ( 'Session',              'https://gitlab.com/DesQ/Session.git' ),
    'Shell':            ( 'Shell',                'https://gitlab.com/DesQ/Shell.git' ),
    'ShellPlugins':     ( 'Plugins/ShellPlugins', 'https://gitlab.com/DesQ/DesQPlugins/ShellPlugins.git' ),
    'Theme':            ( 'Theme',                'https://gitlab.com/DesQ/Theme.git' ),
    'Integration':      ( 'Integration',          'https://gitlab.com/DesQ/Integration.git' ),
    'Clipboard':        ( 'Utils/Clipboard',      'https://gitlab.com/DesQ/DesQUtils/Clipboard.git' ),
    'Disks':            ( 'Utils/Disks',          'https://gitlab.com/DesQ/DesQUtils/Disks.git' ),
    "Keyring":          ( "Utils/Keyring",        'https://gitlab.com/DesQ/DesQUtils/Keyring.git' ),
    "Lock":             ( "Utils/Lock",           'https://gitlab.com/DesQ/DesQUtils/Lock.git' ),
    'Notifier':         ( 'Utils/Notifier',       'https://gitlab.com/DesQ/DesQUtils/Notifier.git' ),
    'PolkitExec':       ( 'Utils/PolkitExec',     'https://gitlab.com/DesQ/DesQUtils/PolkitExec.git' ),
    'PowerManager':     ( 'Utils/PowerManager',   'https://gitlab.com/DesQ/DesQUtils/PowerManager.git' ),
    'Runner':           ( 'Utils/Runner',         'https://gitlab.com/DesQ/DesQUtils/Runner.git' ),
    'SNI':              ( 'Utils/SNI',            'https://gitlab.com/DesQ/DesQUtils/SNI.git' ),
    'Splash':           ( 'Utils/Splash',         'https://gitlab.com/DesQ/DesQUtils/Splash.git' ),
    'Volume':           ( 'Utils/Volume',         'https://gitlab.com/DesQ/DesQUtils/Volume.git' ),
    'Archiver':         ( 'Apps/Archiver',        'https://gitlab.com/DesQ/DesQApps/DesQArchiver.git' ),
    'DropDown':         ( 'Apps/DropDown',        'https://gitlab.com/DesQ/DesQApps/DesQDropDown.git' ),
    'Docs':             ( 'Apps/Docs',            'https://gitlab.com/DesQ/DesQApps/DesQDocs.git' ),
    'Term':             ( 'Apps/Term',            'https://gitlab.com/DesQ/DesQApps/DesQTerm.git' ),
    'Eye':              ( 'Apps/Eye',             'https://gitlab.com/DesQ/DesQApps/DesQEye.git' ),
}

def uninstall( BuildDir ) :

    for proj in Project:
        if not os.path.exists( f"{ProjectsInfo[proj][0]}/{BuildDir}/meson-logs/install-log.txt" ):
            print( f"{proj} not installed. Nothing to uninstall." )
            continue

        os.system( f"cd {ProjectsInfo[proj][0]}; sudo ninja -C .build uninstall" )


if __name__ == '__main__' :

    import argparse

    parser = argparse.ArgumentParser( description = 'Compile and install the DesQ Project', add_help = False )
    parser.add_argument( "-h", '--help', help = 'Print this help and exit', action = "help" )
    parser.add_argument(
        "-i", '--install',
        help = "Install the project.",
        dest = "install",
        action = "store_true"
    )

    parser.add_argument(
        "-u", '--uninstall',
        help = "Uninstall the project.",
        dest = "uninstall",
        action = "store_true"
    )

    parser.add_argument(
        "-p", '--prefix',
        help = "Install everything to <prefix>. Defaults to '/usr'. Provide absolute path.",
        dest = "prefix",
        default = "/usr"
    )

    parser.add_argument(
        "-c", '--clean',
        help = "Remove the existing build directory before compilation.",
        dest = "clean",
        action = "store_true",
    )

    parser.add_argument(
        "-q", '--qt-version',
        help = "Choose the Qt version to use for compilation.",
        dest = "qtver",
        default = "qt6"
    )

    args = parser.parse_args()

    # By default, we compile for Qt6
    QtVer = "-Duse_qt_version=qt6"
    Build = ".build"

    # The user wants Qt5
    if args.qtver in ["qt5", "5"]:
        QtVer = "-Duse_qt_version=qt5"
        Build = ".build5"

    if ( not args.install and not args.uninstall ):
        print( "Please specify at least one of --install or --uninstall\n" )

        parser.print_help()

    elif args.install :
        ChosenProjects = []

        reply = input(
        	"Choose the packages you want to install:\n"
        	"   1. libdesq        2. libdesqui      3. Session\n"       +
        	"   4. Shell          5. Integration    6. Theme\n"         +
        	"   7. Clipboard      8. Disks          9. Keyring\n"       +
        	"  10. Lock          11. Notifier      12. PowerManager\n"  +
        	"  13. Runner        14. SNI           15. Volume\n"        +
        	"  16. Archiver      17. DropDown      18. Docs\n"          +
        	"  19. Term          20. Eye\n"                             +
        	"Your choice (comma separated values | 0 for all):"
        )

        choices = reply.replace( " ", "" ).replace( ",", " " ).strip().split( " " )
        if '0' in choices:
            ChosenProjects = Projects[:]

        else:
            for choice in set( choices ):
                idx = int( choice )
                if idx < 0 or idx >= len( Projects ):
                    print( "Skipping invalid input: ", idx )
                    continue

                ChosenProjects.append( Projects[ idx ] )

        for proj in ChosenProjects:
            ## We don't need to compile Meta.
            if proj == "Meta":
                continue

            print( proj )

            # This project is not cloned yet: Clone it.
            if not os.path.exists( ProjectsInfo[ proj ][ 0 ] ):
                os.system( f"git clone {ProjectsInfo[ proj ][ 1 ]} {ProjectsInfo[ proj ][ 0 ]}" )
                os.chdir( ProjectsInfo[ proj ][ 0 ] )

            # This project exists: Update it.
            else:
                os.chdir( ProjectsInfo[ proj ][ 0 ] )
                os.system( f"git checkout main; git pull --recurse-submodules --all" )

            # If the build directory exists, and we are tasked to clean it.
            if os.path.exists( Build ) and args.clean:
                shutil.rmtree( Build )

            # No options available
            if os.path.exists( "meson_options.txt" ) == False :
                QtVer = ""

            ret = 0
            if not os.path.exists( Build ):
                while True:
                    ret = os.system( f"meson setup {Build} --prefix={args.prefix} --buildtype=release {QtVer}" )
                    if not ret:
                        break;

                    input( "Errors encountered while running meson. Review the code, create or make edits to meson.build and hit Enter to proceed." )

            # Proceed only if compilation was successful.
            skip = False
            while True:
                ret = os.system( f"ninja -C {Build} -k 0 -j $(nproc)" )
                if not ret:
                    break;

                reply = input(
                    "Errors encountered while building. Choose what to do:\n" +
                    "  1. Review the code, make edits and build the project again.\n" +
                    "  2. Skip this project and continue to next one.\n " +
                    "Your choice [1|2]: "
                )

                # Breaking from this loop => Skip
                if reply == '2':
                    skip = True
                    break

                else:
                    continue

            # Do not try to install if we are skipping this package.
            if skip == False:
                # Proceed only if installation was successful.
                while True:
                    ret = os.system( f"sudo ninja -C {Build} install" )
                    if ( not ret ):
                        break;

                    input( "Errors encountered while installing. Review the install instructions and hit Enter to proceed." )
                    reply = input(
                        "Errors encountered while installing. Choose what to do:\n" +
                        "  1. Review the install instructions and re-attempt installation.\n" +
                        "  2. Skip this project and continue to next one.\n " +
                        "Your choice [1|2]: "
                    )

                    # Breaking from this loop => Skip
                    if reply == '2':
                        break

            os.chdir( '../' )
            if ( '/' in ProjectsInfo[ proj ][ 0 ] ):
                os.chdir( '../' )

            print()


    elif args.uninstall :
        uninstall( Build )
