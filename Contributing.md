# Contributions

Contributions do not exclusively mean code. Documentation, examples and bug reports are also very important contributions.

1. Documentation incudes, but is not limited to.
    - A brief summary of the class/function/variables etc.
    - Explanation of some algorithms.
    - Short code snippets in the form of comments.
   In short anything that helps some one to understand the code better is good documentation.

2. Examples are meant to demonstrate the use of a class, or code sequences to achieve a result. Examples
    - Should be complete, and working.
    - Should follow the code style; please run uncrustify using the configuration file provided in this repo.
    - Should preferably have comments explaining the code.

3. Bug reports help in improving the quality of the project. it is easy to make mistakes in a large project like this.
    - Bug reports should be comprehensive.
    - Bug reports should clearly state the problem with the current code/beahviour.
    - It should also state what is the expected behaviour.
    - Stacktraces are most welcome, but should be posted as a link to some service like pastebin.
    - Feel free to open merge requests that fix the bug, in case you have the expertise.
    - Code snippets are welcome, but MRs are preferable.
