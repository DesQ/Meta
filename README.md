# Meta
A simple project which contains files/resources that may be used across the DesQ Group. It's also used as a generic issues tracker.

## Resources
 - Uncrustify - All contributors are requested to run uncrustify using [this](https://gitlab.com/DesQ/Meta/-/blob/main/uncrustify.cfg) configuration file so that we may have a uniform code style.
 - Compile script - A simple pythonic [compile](https://gitlab.com/DesQ/Meta/-/blob/main/compile.py) script is available to compile and install the entire DesQ Project.

## Issues Tracker

Issues that may be opened here:
 - Issues that may not belong to any specific project (ex: a request for a new desq utility)
 - Issues that may belong to multiple projects (ex: problems regarding uncrustify).
 - If the user does not know in which project to open an issue, they may do so here.
 - Problems with the resources need to be reported here.

Please note: Issues opened here may be moved to suitable project without prior notice. In such cases, the said issue will be closed here and a new issue will be created in the taget project.

